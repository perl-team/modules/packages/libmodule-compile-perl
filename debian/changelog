libmodule-compile-perl (0.38-3) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.7.0, no changes.
  * Enable Salsa CI.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 07 Mar 2025 08:34:28 +0100

libmodule-compile-perl (0.38-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.1, no changes.
  * Add Rules-Requires-Root to control file.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 30 Nov 2022 15:28:31 +0000

libmodule-compile-perl (0.38-1) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.4.1, no changes.
  * Update gbp.conf to use --source-only-changes by default.

  [ gregor herrmann ]
  * Import upstream version 0.38.
  * Refresh patches (fuzz).
  * Update years of upstream copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- gregor herrmann <gregoa@debian.org>  Thu, 26 Dec 2019 03:13:28 +0100

libmodule-compile-perl (0.37-1) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * New upstream release.
  * Add gbp.conf to use pristine-tar by default.
  * Bump Standards-Version to 4.1.4, no changes.
  * Update Vcs-* URLs for Salsa.
  * Fix typo in patch description.
  * Update copyright years for Ingy döt Net.
  * Add libcapture-tiny-perl to build dependencies.
  * Drop 02-spelling-error-in-manpage.patch, applied upstream.
    Refresh remaining patches.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 02 May 2018 21:30:35 +0200

libmodule-compile-perl (0.35-1) unstable; urgency=low

  * Initial Release. (Closes: #827544)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Tue, 21 Jun 2016 03:01:38 +0100
